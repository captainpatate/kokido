#define RELAY_ONOFF 11 // D11
#define RELAY_BWFW 12 // D12
#define SECONDS(x) (x*1000)
#define RELAY_STABILIZE_DURATION 1000 //relay goes crazy with 500?

#define PUSH_BUTTON_PIN 2

const unsigned long CYCLE_DURATION = 30000UL;
unsigned int count;

void togglePin(int pin) {
  digitalWrite(pin, !digitalRead(pin));
}

void changeDirection() {
  digitalWrite(RELAY_ONOFF, HIGH);
  delay(RELAY_STABILIZE_DURATION); // wait for stabilizatoin of the first relay
  togglePin(RELAY_BWFW);
  delay(RELAY_STABILIZE_DURATION);
  digitalWrite(RELAY_ONOFF, LOW);
}

// the setup function runs once when you press reset or power the board
void setup() {
  pinMode(PUSH_BUTTON_PIN, INPUT_PULLUP);
  pinMode(RELAY_ONOFF, OUTPUT);
  pinMode(RELAY_BWFW, OUTPUT);
  Serial.begin(9600);
  Serial.println("===========");
  count = 1;
}

// the loop function runs over and over again forever
void loop() {
  changeDirection();
  Serial.print("Waiting ");
  Serial.print((unsigned long)CYCLE_DURATION * count);
  Serial.print(", count=");
  Serial.println(count);
  delay((unsigned long)CYCLE_DURATION * count);

  if (count == 3) {
    count = 1;
  }
  else {
    count = count + 1;
  }
}
